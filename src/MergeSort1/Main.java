package MergeSort1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in); 
		System.out.println("Enter length"); 
		int length = in.nextInt();
		 
		int[] arr = new int[length];
		
		System.out.println("Enter " + length + " numbers"); 
		for(int i = 0;i<length;i++) { 
			arr[i] = in.nextInt(); 
		}
		mergeSort(arr,0,length-1);
		displayArray(arr);

	}
	public static void merge(int[] arr,int l,int m,int r ) { 
		int length1 = m - l + 1;
		int length2 = r - m;
		int[] arr1 = new int[length1];
		int[] arr2 = new int[length2];
		int index = l;
		
		for(int i = 0;i<length1;i++) {
			arr1[i] = arr[index];
			index++;
		}
		for(int j = 0;j<length2;j++) {
			arr2[j] = arr[index];
			index++;
		}
		
		int i = 0;
		int j = 0;
		int k = l;
		
		while ((i < length1) && (j < length2)) {
			if(arr1[i] < arr2[j]) {
				arr[k] = arr1[i];
				i++;
			}
			else {
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}
		while(i < length1) {
			arr[k] = arr1[i];
			i++;
			k++;
		}	
		
		while(j < length2) {
			arr[k] = arr2[j];		

			j++;
			k++;
		}	
	}
	
	public static void mergeSort(int[]arr,int l,int r) {
		
		if(l < r) {
			int m = (l + r) / 2;
			mergeSort(arr, l,m);
			mergeSort(arr,m+1,r);
			merge(arr, l, m, r);
		}
		
		
	}
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}

}
